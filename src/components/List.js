import React, { useEffect, useState } from "react";
import "../assets/list.css";
import axios from "axios";
const List = () => {
  const [gameList, setGameList] = useState([]);
  const [order, setOrder] = useState("ASC");

  const sorting = (col) => {
    if (order === "ASC") {
      const sorted = [...gameList].sort((a, b) =>
        a[col].toLowerCase() > b[col].toLowerCase() ? 1 : -1
      );
      setGameList(sorted);
      setOrder("DSC");
    }
  };
  const baseURL =
    "https://s3-ap-southeast-1.amazonaws.com/he-public-data/gamesarena274f2bf.json";

  useEffect(() => {
    async function getGameData() {
      const response = await axios.get(baseURL).then((response) => {
        return response;
      });
      let mb = response.data.shift();
      setGameList(response.data);
    }
    getGameData();
  }, []);

  const onInputTextChange = (event) => {
    const searchData = gameList.filter((ele) => {
      return ele.title.toLowerCase().includes(event.target.value.toLowerCase());
    });
    setGameList(searchData);
  };
  const search =
    gameList.length !== 0 ? (
      gameList.map((user, key) => {
        return (
          <tr className="col" key={Math.random()}>
            <td>{user.title}</td>
            <td>{user.platform}</td>
            <td>{user.score}</td>
            <td>{user.genre}</td>
            <td>{user.editors_choice}</td>
          </tr>
        );
      })
    ) : (
      <tr>
        <td>
          <p>Error</p>
        </td>
      </tr>
    );
  return (
    <>
      <div className="container mx-2 my-3">
        <div className="mx-2 my-2">
          <input
            type="text"
            placeholder="search.."
            onChange={onInputTextChange}
          ></input>
        </div>
        <div className="mx-2 my-2">
          <button onClick={() => sorting("platform")}>Sort List</button>
        </div>
        <table>
          <tbody>
            <tr>
              <th>title</th>
              <th>platform</th>
              <th>score</th>
              <th>genre</th>
              <th>editors_choice</th>
            </tr>
            {search}
          </tbody>
        </table>
      </div>
    </>
  );
};
export default List;
